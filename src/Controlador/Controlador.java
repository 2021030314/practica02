
package Controlador;
import Modelo.Cotizacion;
import Vista.dlgCotizaciones;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Controlador implements ActionListener{
    private Cotizacion COT;
    private dlgCotizaciones vista;

    private Controlador(Cotizacion COT, dlgCotizaciones vista) {
        this.COT = COT;
        this.vista = vista;
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    
    private void iniciarV(){
        vista.setTitle("+|+ CONCESIONARIA +|+");
        vista.setSize(670, 675);
        vista.setVisible(true);
    }
    
    private void disableText(){
        vista.txtDesc.enable(false);
        vista.txtCodigo.enable(false);
        vista.txtPor.enable(false);
        vista.txtPrecio.enable(false);
        vista.comboPlazo.enable(false);
    }
    
    private void enableText(){
        vista.txtDesc.enable(true);
        vista.txtCodigo.enable(true);
        vista.txtPor.enable(true);
        vista.txtPrecio.enable(true);
        vista.comboPlazo.enable(true);
    }
    
    private void limpiar(){
        vista.txtDesc.setText(null);
        vista.txtCodigo.setText(null);
        vista.txtPor.setText(null);
        vista.txtPrecio.setText(null);
        vista.txtMensual.setText(null);
        vista.txtTotal.setText(null);
        vista.txtPago.setText(null);
    }
    
    private void isPorciento_100(){
        float pr = Float.parseFloat(vista.txtPor.getText());
        if(pr > 100){
            JOptionPane.showMessageDialog(vista, "Porcentaje excedido de 100%");
            vista.btnCancelar.doClick();
        }
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnLimpiar){
            limpiar();
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnNuevo){
            enableText();
            vista.btnCancelar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnCancelar){
            vista.btnLimpiar.doClick();
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            disableText();
        }
        
        if(e.getSource() == vista.btnGuardar){
            COT.setCodigo(vista.txtCodigo.getText());
            COT.setDesc(vista.txtDesc.getText());
            vista.btnMostrar.setEnabled(true);
            isPorciento_100();
            
            
            try{       
                COT.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                COT.setPor(Float.parseFloat(vista.txtPor.getText()));
                COT.setPlazo(Integer.parseInt(vista.comboPlazo.getSelectedItem().toString()));
                JOptionPane.showMessageDialog(vista, "¡Agregado de forma exitosa!");
            }        
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }
        }
        
        if(e.getSource() == vista.btnMostrar){
            disableText();
            vista.txtCodigo.setText(COT.getCodigo());
            vista.txtDesc.setText(COT.getDesc());
            vista.txtPor.setText(Float.toString(COT.getPor()));
            vista.txtPrecio.setText(Float.toString(COT.getPrecio()));
            vista.comboPlazo.setSelectedItem((COT.getPlazo()));
            vista.txtMensual.setText(Float.toString(COT.calculoMensual()));
            vista.txtTotal.setText(Float.toString(COT.calculoTotal()));
            vista.txtPago.setText(Float.toString(COT.calculoPago()));
            vista.btnLimpiar.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnCerrar){
            int out = JOptionPane.showConfirmDialog(vista, "¿Desea salir del programa?", "CERRAR PROGRAMA",JOptionPane.OK_CANCEL_OPTION);
            if(out == JOptionPane.OK_OPTION){
                this.vista.setVisible(false);
                this.vista.dispose();
                System.exit(0); 
            }
        }
        
        if(e.getSource() == vista.btnLimpiar){
            limpiar();
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(true);
        }
        
        if(e.getSource() == vista.btnCancelar){
            vista.btnLimpiar.doClick();
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            disableText();
        }
    }
    
    public static void main(String[] args) {
        Cotizacion COT = new Cotizacion();
        dlgCotizaciones vista = new dlgCotizaciones(new JFrame(), true);
        Controlador contra = new Controlador(COT, vista);
        contra.iniciarV();
        
    }
}
