/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author felip
 */
public class Cotizacion {
    String numCot;
    String desc;
    float precio;
    float porcentaje;
    int plazo;
    
    public Cotizacion(){
        
    }
    
    public Cotizacion(String numCot, String desc, float precio, float porcentaje, int plazo){
        this.numCot = numCot;
        this.desc = desc;
        this.precio = precio;
        this.porcentaje = porcentaje;
        this.plazo = plazo;
    }
    
    public Cotizacion(Cotizacion COT){
        this.numCot = COT.numCot;
        this.desc = COT.desc;
        this.precio = COT.precio;
        this.porcentaje = COT.porcentaje;
        this.plazo = COT.plazo;
    }
    
    public void setCodigo(String numCot){
        this.numCot = numCot;
    }
    
    public void setDesc(String desc){
        this.desc = desc;
    }
    
    public void setPrecio(float precio){
        this.precio = precio;
    }
    
    public void setPor(float porcentaje){
        this.porcentaje = porcentaje;
    }
    
    public void setPlazo(int plazo){
        this.plazo = plazo;
    }
    
    public String getCodigo(){
        return this.numCot;
    }
    
    public String getDesc(){
        return this.desc;
    }
    
    public float getPrecio(){
        return this.precio;
    }
    
    public float getPor(){
        return this.porcentaje;
    }
    
    public int getPlazo(){
        return this.plazo;
    }
    
    public float calculoPago(){
        return this.precio * (this.porcentaje / 100);
    }
    
    public float calculoTotal(){
        return this.precio - this.calculoPago();
    }
    
    public float calculoMensual(){
        return this.calculoTotal() / this.plazo;
    }
}
